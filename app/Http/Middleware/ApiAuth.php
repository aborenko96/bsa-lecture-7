<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class ApiAuth extends Middleware
{
    public function handle($request, Closure $next, ...$guards)
    {
        if(!Auth::user()) {
            return response()->json(['code' => 401, 'message' => 'Unauthorized'])->setStatusCode(401);
        }
        return parent::handle($request, $next, $guards);
    }

}
