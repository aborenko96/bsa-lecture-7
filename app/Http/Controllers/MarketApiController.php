<?php

namespace App\Http\Controllers;

use App\Http\Resources\ItemResource;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Services\MarketService;
use Exception;
use Illuminate\Support\Facades\Validator;

class MarketApiController extends Controller
{
    private $marketService;

    public function __construct(MarketService $marketService)
    {
        $this->marketService = $marketService;
        $this->middleware('api.auth')->except(['showList', 'showProduct']);
    }

    public function showList()
    {
        return new JsonResponse(ItemResource::collection($this->marketService->getProductList()));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_name' => 'required',
            'product_price' => 'numeric|min:0.01'
        ]);

        if ($validator->fails()) {
            return response()->json(['code' => 400, 'message' => $validator->errors()])->setStatusCode(400);
        }

        $product = $this->marketService->storeProduct($request);

        return (new JsonResponse(new ItemResource($product)))->setStatusCode(201);

    }

    public function showProduct(int $id)
    {
        try {
            return new JsonResponse(new ItemResource($this->marketService->getProductById($id)));
        } catch (Exception $e) {
            return response()->json(['code' => 404, 'message' => 'Not found'])->setStatusCode(404);
        }
    }

    public function delete(Request $request)
    {
        try {
            $this->authorize('delete', $this->marketService->getProductById($request->id));
            $this->marketService->deleteProduct($request);
            return response()->json(['code' => 204])->setStatusCode(204);
        } catch (AuthorizationException $e) {
            return response()->json(['code' => 403, 'message' => 'Permission denied'])->setStatusCode(403);
        } catch (Exception $e) {
            return response()->json(['code' => 404, 'message' => 'Not found'])->setStatusCode(404);
        }
    }

    public function showUserProducts(Request $request)
    {
        $user = $request->user();

        $products = $this->marketService->getProductsByUserId($user->id);

        return new JsonResponse(ItemResource::collection($products));
    }

}
