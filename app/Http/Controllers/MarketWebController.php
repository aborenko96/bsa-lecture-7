<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\MarketService;
use Illuminate\Auth\Access\AuthorizationException;

class MarketWebController extends Controller
{
    private $marketService;

    public function __construct(MarketService $marketService)
    {
        $this->marketService = $marketService;
        $this->middleware('auth')->except(['showMarket', 'showProduct']);
    }

    public function showMarket() {
        $products = $this->marketService->getProductList();

        return view('market', compact('products'));
    }

    public function showProduct(int $id) {
        try {
            $product = $this->marketService->getProductById($id);
        } catch (\Exception $e) {
            return redirect()->route('main');
        }

        return view('product', compact('product'));
    }

    public function addProductForm() {
        return view('addProductForm');
    }

    public function storeProduct(Request $request) {
        $this->marketService->storeProduct($request);

        return redirect()->route('main');
    }

    public function deleteProduct(Request $request) {
        try {
            $this->authorize('delete', $this->marketService->getProductById($request->id));
            $this->marketService->deleteProduct($request);
        }catch (AuthorizationException | \Exception $e){
            return redirect()->route('main');
        }
        
        return redirect()->route('main');
    }
}