<?php

namespace App\Policies;

use App\Entities\Product;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;

    public function delete(User $user, Product $product)
    {
        return $this->isUsersProduct($user, $product);
    }

    private function isUsersProduct(User $user, Product $product)
    {
        return $product->user_id === $user->id;
    }
}
