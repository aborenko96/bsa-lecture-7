<?php

namespace Tests\Feature;

use App\Entities\Product;
use App\Services\MarketService;
use Tests\MarketTest;

class MarketWebTest extends MarketTest
{
    public function testGetAll()
    {
        $mock = $this->mock;

        $mock->method('findAll')
            ->willReturn($this->products);

        $service = new MarketService($mock);

        $this->assertCount(5, $service->getProductList());
    }

    public function testCanDeleteOnlyOwnProduct()
    {
        $user = $this->users[0];

        $this->assertFalse($user->can('delete', $this->products[2]));
        $this->assertTrue($user->can('delete', $this->products[4]));
    }

    public function testStore()
    {
        $mock = $this->mock;

        $product = new Product([
            'name' => 'Product 1',
            'price' => 15,
            'user_id' => $this->users[2]->id
        ]);

        $storedProduct = null;

        $mock->method('store')
            ->will($this->returnCallback(function () use (&$storedProduct) {
                $storedProduct = func_get_arg(0);
            }));

        $this->actingAs($this->users[2])->post('/items', ['product_name' => 'Product 1', 'product_price' => 15]);

        $this->assertEquals($storedProduct->user_id, $product->user_id);

    }

    public function testGetProduct()
    {
        $product = new Product([
            'id' => 2,
            'user_id' => 3,
            'name' => 'Product 2',
            'price' => 3.51
        ]);

        $mock = $this->mock;

        $service = new MarketService($mock);

        $mock->method('findById')
            ->with($product->id)
            ->willReturn($this->products->where('id', $product->id)->first());

        $productFromRepo = $service->getProductById($product->id);

        $this->assertEquals($product->id, $productFromRepo->id);
        $this->assertEquals($product->price, $productFromRepo->price);
        $this->assertEquals($product->user_id, $productFromRepo->user_id);
    }

    public function testGetUsersProducts()
    {
        $products = $this->products->where('user_id', $this->users[2]->id);

        $mock = $this->mock;

        $service = new MarketService($mock);

        $mock->method('findByUserId')
            ->with($this->users[2]->id)
            ->willReturn($products);

        $this->assertCount($products->count(), $service->getProductsByUserId($this->users[2]->id)->toArray());
        $this->assertEquals($products->toArray(), $service->getProductsByUserId($this->users[2]->id)->toArray());

    }
}
