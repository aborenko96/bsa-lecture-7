<?php

namespace Tests\Feature;

use App\Entities\Product;
use App\Repositories\Interfaces\ProductRepositoryInterface;
use Tests\MarketTest;

class MarketApiTest extends MarketTest
{
    public function testGetAll()
    {
        $mock = $this->mock;

        $mock->method('findAll')
            ->willReturn($this->products);

        $this->app->instance(ProductRepositoryInterface::class, $mock);

        $response = $this->actingAs($this->users[1])->get('/api/items');
        $response->assertStatus(200);
        $response->assertJson($this->products->toArray());

    }

    public function testCanDeleteOnlyOwnProduct()
    {
        $user = $this->users[0];

        $mock = $this->mock;

        $mock->method('findById')
            ->will($this->returnValueMap([
                [3, $this->products[2]],
                [5, $this->products[4]]
            ]));

        $mock->method('delete');

        $response = $this->actingAs($user)->delete('/api/items/' . $this->products[2]->id);
        $response->assertStatus(403);

        $response = $this->actingAs($user)->delete('/api/items/' . $this->products[4]->id);
        $response->assertStatus(204);
    }

    public function testStore()
    {
        $mock = $this->mock;

        $product = new Product([
            'name' => 'Product 1',
            'price' => 15,
            'user_id' => $this->users[2]->id
        ]);

        $mock->method('store')
            ->willReturn($product);

        $this->app->instance(ProductRepositoryInterface::class, $mock);

        $response = $this->actingAs($this->users[2])->post('/api/items', ['product_name' => 'Product 1', 'product_price' => 15]);
        $response->assertStatus(201);
        $response->assertJson($product->toArray());

        $response = $this->actingAs($this->users[2])->post('/api/items', ['product_name' => 'Product 1', 'product_price' => -1]);
        $response->assertStatus(400);
    }

    public function testGetProduct()
    {
        $product = new Product([
            'id' => 2,
            'user_id' => 3,
            'name' => 'Product 2',
            'price' => 3.51
        ]);

        $mock = $this->mock;

        $mock->method('findById')
            ->with($product->id)
            ->willReturn($this->products->where('id', $product->id)->first());

        $response = $this->actingAs($this->users[2])->get('/api/items/' . $product->id);
        $response->assertStatus(200);
        $response->assertJson($product->toArray());

        $response = $this->actingAs($this->users[2])->get('/api/items/150');
        $response->assertStatus(404);
    }

    public function testGetUsersProducts()
    {
        $products = $this->products->where('user_id', $this->users[2]->id);

        $mock = $this->mock;

        $mock->method('findByUserId')
            ->with($this->users[2]->id)
            ->willReturn($products);

        $response = $this->actingAs($this->users[2])->get('/api/my-items');

        $response->assertJson(array_values($products->toArray()));
        $response->assertStatus(200);

    }

    public function testDenyUnauthorized()
    {
        $response = $this->get('/api/my-items');

        $response->assertJson([
            'code' => 401,
            'message' => 'Unauthorized'
        ]);

        $response->assertStatus(401);
    }
}
