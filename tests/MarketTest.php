<?php

namespace Tests;

use App\Entities\Product;
use App\Repositories\Interfaces\ProductRepositoryInterface;
use App\User;

class MarketTest extends TestCase
{
    protected $products;
    protected $users;
    protected $mock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->users = [
            factory(User::class)->make(['id' => 1]),
            factory(User::class)->make(['id' => 2]),
            factory(User::class)->make(['id' => 3])
        ];

        $this->products = collect([
            new Product([
                'id' => 1,
                'name' => 'Product 1',
                'price' => 5,
                'user_id' => 2
            ]),
            new Product([
                'id' => 2,
                'name' => 'Product 2',
                'price' => 3.51,
                'user_id' => 3
            ]),
            new Product([
                'id' => 3,
                'name' => 'Product 3',
                'price' => 35.7,
                'user_id' => 3
            ]),
            new Product([
                'id' => 4,
                'name' => 'Product 4',
                'price' => 15.1,
                'user_id' => 2
            ]),
            new Product([
                'id' => 5,
                'name' => 'Product 5',
                'price' => 25.2,
                'user_id' => 1,
            ])
        ]);

        $this->mock = $this->createMock(ProductRepositoryInterface::class);
        $this->app->instance(ProductRepositoryInterface::class, $this->mock);
    }
}
